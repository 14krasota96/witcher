//= ../../node_modules/jquery/dist/jquery.js

addParallax();

function addParallax() {
    document.addEventListener('mousemove', parallax);
    
    function parallax(e) {
        this.querySelectorAll('.sparks').forEach(layer => {
            let speed = layer.getAttribute('data-speed');
            let x = (window.innerWidth - e.pageX * speed)/100;
            let y = (window.innerWidth - e.pageY * speed)/100;

            layer.style.transform = `translate(${x}px, ${y}px)`
        })
    }
}

function saveInClipBoard() {

    let copyText = document.getElementById('textId');
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    document.execCommand('copy');
    setTimeout( copyComplete, 1000);

}

function copyComplete() {
    alert('Copied the text: WITCHER');
}
